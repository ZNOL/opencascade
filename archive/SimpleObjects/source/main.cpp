#include <STEPExport.hxx>

#include "sphere.h"

int32_t main() {
  auto obj1 = CreateSphere(30, 0, 0, 0);

  OCCUtils::STEP::ExportSTEP(obj1, "sphere with holes.step");

  auto obj2 = CreateParallelepiped(20, 20, 40);

  OCCUtils::STEP::ExportSTEP(obj2, "parallelepiped.step");
}