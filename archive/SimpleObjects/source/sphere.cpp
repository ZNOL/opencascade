#include "sphere.h"

TopoDS_Shape CreateSphere(uint32_t radius, int32_t x, int32_t y, int32_t z) {
  gp_Pnt center(x, y, z);
  gp_Pnt bottom(x, y, z - static_cast<double>(radius));
  gp_Pnt left(x - static_cast<double>(radius), y, z);

  BRepPrimAPI_MakeSphere mkSphere(center, radius);

  gp_Dir upDir(0, 0, 1);
  gp_Dir leftDir(1, 0, 0);
  gp_Ax2 upAxis(bottom, upDir);
  gp_Ax2 leftAxis(left, leftDir);

  BRepPrimAPI_MakeCylinder mkCylinderUp(upAxis, static_cast<double>(radius) / 4.0, 2 * radius);
  BRepPrimAPI_MakeCylinder mkCylinderLeft(leftAxis, static_cast<double>(radius) / 4.0, 2 * radius);

  auto sphere1 = BRepAlgoAPI_Cut(mkSphere.Shape(), mkCylinderUp.Shape());
  auto sphere = BRepAlgoAPI_Cut(sphere1, mkCylinderLeft.Shape());

  return sphere;
}

TopoDS_Face MakeCircle(gp_Pnt A, Standard_Real radius) {
  auto x = radius / 1.4142;

  gp_Pnt B(A.X() + x, A.Y() + x, A.Z());
  gp_Pnt C(A.X(), A.Y() + x, A.Z());
  gp_Pnt D(A.X() + x, A.Y(), A.Z());

  Handle(Geom_TrimmedCurve) arc1 = GC_MakeArcOfCircle(A, C, B);
  Handle(Geom_TrimmedCurve) arc2 = GC_MakeArcOfCircle(A, D, B);

  TopoDS_Edge edge1 = BRepBuilderAPI_MakeEdge(arc1);
  TopoDS_Edge edge2 = BRepBuilderAPI_MakeEdge(arc2);
  TopoDS_Wire wire = BRepBuilderAPI_MakeWire(edge1, edge2);

   return BRepBuilderAPI_MakeFace(wire);
}

TopoDS_Shape CreateParallelepiped(uint32_t a, uint32_t b, uint32_t c) {
  gp_Pnt xoyoz(0, 0, 0);

  BRepPrimAPI_MakeBox mkBox(xoyoz, a, b, c);

  gp_Dir dir(10, 10, 10);
  gp_Dir dir2(0, 0, 10);
  auto circle1 = MakeCircle({0, 0, 0}, 10);
  auto circle2 = MakeCircle({0, 0, 30}, 20);
  auto circle3 = MakeCircle({a / 2.0, b / 2.0, 0}, 5);

  BRepPrimAPI_MakePrism mkPrism1(circle1, dir);
  BRepPrimAPI_MakePrism mkPrism2(circle2, dir);
  BRepPrimAPI_MakePrism mkPrism3(circle3, dir2);

  auto box = mkBox.Shape();
  box = BRepAlgoAPI_Cut(box, mkPrism1.Shape());
  box = BRepAlgoAPI_Cut(box, mkPrism2.Shape());
  box = BRepAlgoAPI_Cut(box, mkPrism3.Shape());

  return box;
}
