#ifndef SIMPLEOBJECTS_SPHERE_H
#define SIMPLEOBJECTS_SPHERE_H

#include <TopoDS.hxx>

#include <gp_Ax2.hxx>
#include <gp_Ax3.hxx>

#include <GC_MakeArcOfCircle.hxx>

#include <gp_Sphere.hxx>

#include <BRepPrimAPI_MakeSphere.hxx>
#include <BRepPrimAPI_MakeCylinder.hxx>
#include <BRepPrimAPI_MakeBox.hxx>
#include <BRepPrimAPI_MakePrism.hxx>

#include <BRepBuilderAPI_MakeEdge.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>

#include <BRepAlgoAPI_Cut.hxx>
#include <BRepAlgoAPI_Common.hxx>
#include <BRepAlgoAPI_Fuse.hxx>


TopoDS_Shape CreateSphere(uint32_t, int32_t, int32_t, int32_t);

TopoDS_Shape CreateParallelepiped(uint32_t, uint32_t, uint32_t);

#endif //SIMPLEOBJECTS_SPHERE_H
