#include "bottle.h"

#include <cstdint>

#include <STEPExport.hxx>

int32_t main() {
  int32_t Width = 50, Thickness = 30, Height = 70;

  auto bottle = MakeBottle(Width, Thickness, Height);

  OCCUtils::STEP::ExportSTEP(bottle, "bottle_shape.step");

  return 0;
}
