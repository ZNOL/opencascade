#include <bottle.h>

TopoDS_Shape MakeBottle(int32_t Width, int32_t Thickness, int32_t Height) {
  gp_Pnt aPnt1(- Width / 2.0, 0, 0);
  gp_Pnt aPnt2(- Width / 2.0, - Thickness / 4.0, 0);
  gp_Pnt aPnt3(0, - Thickness / 2.0, 0);
  gp_Pnt aPnt4(Width / 2.0, - Thickness / 4.0, 0);
  gp_Pnt aPnt5(Width / 2.0, 0, 0);
  // -----------------------------------------------------------------------------
  Handle(Geom_TrimmedCurve) arcOfCircle = GC_MakeArcOfCircle(aPnt2, aPnt3, aPnt4);
  Handle(Geom_TrimmedCurve) segment1 = GC_MakeSegment(aPnt1,aPnt2);
  Handle(Geom_TrimmedCurve) segment2 = GC_MakeSegment(aPnt4,aPnt5);
  // -----------------------------------------------------------------------------
  TopoDS_Edge edge1 = BRepBuilderAPI_MakeEdge(segment1);     // or = BRepBuilderAPI_MakeEdge(aPnt1, aPnt2);
  TopoDS_Edge edge2 = BRepBuilderAPI_MakeEdge(arcOfCircle);
  TopoDS_Edge edge3 = BRepBuilderAPI_MakeEdge(segment2);     // or = BRepBuilderAPI_MakeEdge(aPnt4, aPnt5);
  TopoDS_Wire wire = BRepBuilderAPI_MakeWire(edge1, edge2, edge3);
  // -----------------------------------------------------------------------------
  gp_Trsf trsf_reflection;
  trsf_reflection.SetMirror(gp::OX());

  BRepBuilderAPI_Transform BRepTrsf(wire, trsf_reflection);
  TopoDS_Shape shapeMirrored = BRepTrsf.Shape();
  TopoDS_Wire wireMirrored = TopoDS::Wire(shapeMirrored);

  BRepBuilderAPI_MakeWire mkWire;
  mkWire.Add(wire);
  mkWire.Add(wireMirrored);
  TopoDS_Wire bottomWire = mkWire.Wire();
  // -----------------------------------------------------------------------------
  TopoDS_Face bottomFace = BRepBuilderAPI_MakeFace(bottomWire);
  gp_Vec prismVec(0, 0, Height);

  TopoDS_Shape myBody = BRepPrimAPI_MakePrism(bottomFace, prismVec);
  // -----------------------------------------------------------------------------
  BRepFilletAPI_MakeFillet mkFillet(myBody);

  TopExp_Explorer exp;
  for (exp.Init(myBody, TopAbs_EDGE); exp.More(); exp.Next()) {
    auto curEdge = TopoDS::Edge(exp.Current());

    mkFillet.Add(Thickness / 12.0, curEdge);
  }
  myBody = mkFillet.Shape();
  // -----------------------------------------------------------------------------
  gp_Pnt neckLocation(0, 0, Height);
  gp_Dir necAxis = gp::DZ();
  gp_Ax2 necAx2(neckLocation, necAxis);
  auto neckRadius = Thickness / 4.0;
  auto neckHeight = Height / 10.0;
  BRepPrimAPI_MakeCylinder mkCylinder(necAx2, neckRadius, neckHeight);
  TopoDS_Shape neck = mkCylinder.Shape();
  // -----------------------------------------------------------------------------
  myBody = BRepAlgoAPI_Fuse(myBody, neck);
  // -----------------------------------------------------------------------------
  Standard_Real zMax = -1;
  TopoDS_Face faceToRemove;
  for (TopExp_Explorer faceExp(myBody, TopAbs_FACE); faceExp.More(); faceExp.Next()) {
    auto face = TopoDS::Face(faceExp.Current());
    Handle(Geom_Surface) surface = BRep_Tool::Surface(face);
    if (surface->DynamicType() == STANDARD_TYPE(Geom_Plane)) {
      auto plane = Handle(Geom_Plane)::DownCast(surface);
      if (zMax < plane->Location().Z()) {
        zMax = plane->Location().Z();
        faceToRemove = face;
      }
    }
  }
  if (zMax == -1) {
    throw std::logic_error("there are no plane in bottle");
  }
  TopTools_ListOfShape facesToRemove;
  facesToRemove.Append(faceToRemove);

  BRepOffsetAPI_MakeThickSolid solidMaker;
  solidMaker.MakeThickSolidByJoin(myBody, facesToRemove, -Thickness/50.0, 1.e-3);

  myBody = solidMaker.Shape();
  // -----------------------------------------------------------------------------
  Handle(Geom_CylindricalSurface) aCyl1 = new Geom_CylindricalSurface(necAx2, neckRadius * 0.99);
  Handle(Geom_CylindricalSurface) aCyl2 = new Geom_CylindricalSurface(necAx2, neckRadius * 1.05);

  gp_Pnt2d pnt2D(2.0 * M_PI, neckHeight / 2.0);
  gp_Dir2d dir2D(2.0 * M_PI, neckHeight / 4.0);
  gp_Ax2d ax2D(pnt2D, dir2D);

  auto majorRad = 2.0 * M_PI;
  auto minorRad  = neckHeight / 10.0;
  Handle(Geom2d_Ellipse) ellipse1 = new Geom2d_Ellipse(ax2D, majorRad, minorRad);
  Handle(Geom2d_Ellipse) ellipse2 = new Geom2d_Ellipse(ax2D, majorRad, minorRad / 4.0);

  Handle(Geom2d_TrimmedCurve) arc1 = new Geom2d_TrimmedCurve(ellipse1, 0, M_PI);
  Handle(Geom2d_TrimmedCurve) arc2 = new Geom2d_TrimmedCurve(ellipse2, 0, M_PI);

  gp_Pnt2d ellipsePnt1 = ellipse1->Value(0);
  gp_Pnt2d ellipsePnt2;
  ellipse1->D0(M_PI, ellipsePnt2);

  Handle(Geom2d_TrimmedCurve) segment = GCE2d_MakeSegment(ellipsePnt1, ellipsePnt2);

  TopoDS_Edge edge1OnSurf1 = BRepBuilderAPI_MakeEdge(arc1, aCyl1);
  TopoDS_Edge edge2OnSurf1 = BRepBuilderAPI_MakeEdge(segment, aCyl1);
  TopoDS_Edge edge1OnSurf2 = BRepBuilderAPI_MakeEdge(arc2, aCyl2);
  TopoDS_Edge edge2OnSurf2 = BRepBuilderAPI_MakeEdge(segment, aCyl2);

  TopoDS_Wire threadingWire1 = BRepBuilderAPI_MakeWire(edge1OnSurf1, edge2OnSurf1);
  TopoDS_Wire threadingWire2 = BRepBuilderAPI_MakeWire(edge1OnSurf2, edge2OnSurf2);

  BRepLib::BuildCurves3d(threadingWire1);
  BRepLib::BuildCurves3d(threadingWire2);

  BRepOffsetAPI_ThruSections tool(Standard_True);
  tool.AddWire(threadingWire1);
  tool.AddWire(threadingWire2);
  tool.CheckCompatibility(Standard_False);

  TopoDS_Shape threading = tool.Shape();
  // -----------------------------------------------------------------------------
  TopoDS_Compound result;
  BRep_Builder builder;
  builder.MakeCompound(result);
  builder.Add(result, myBody);
  builder.Add(result, threading);

  return result;
}