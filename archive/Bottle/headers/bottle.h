#ifndef SAMPLE_BOTTLE_BOTTLE_H
#define SAMPLE_BOTTLE_BOTTLE_H

#include <gp_Pnt.hxx> // Points objects
#include <gp_Ax1.hxx> // Axis objects
#include <gp_Vec.hxx> // Vector objects
#include <Geom_Plane.hxx>  // Plane obj

#include <GC_MakeSegment.hxx> // Create Curve from Points
#include <GC_MakeArcOfCircle.hxx>

#include <Standard_Handle.hxx> // Handler for curve
#include <Geom_TrimmedCurve.hxx>  // Curve reference

#include <Geom_CylindricalSurface.hxx>
#include <Geom2d_Ellipse.hxx>
#include <Geom2d_TrimmedCurve.hxx>

#include <GCE2d_MakeSegment.hxx>

#include <TopoDS.hxx> // Union different topology with one object
#include <TopoDS_Edge.hxx>
#include <TopoDS_Wire.hxx>

#include <BRepBuilderAPI_MakeEdge.hxx> // Builders for topology objects
#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>

#include <BRepPrimAPI_MakePrism.hxx>
#include <BRepPrimAPI_MakeCylinder.hxx>

#include <gp_Trsf.hxx> // Transformation objects
#include <BRepBuilderAPI_Transform.hxx> // Transforms topology

#include <BRepFilletAPI_MakeFillet.hxx>  // for making fillets
#include <BRepOffsetAPI_MakeThickSolid.hxx>  // for making thick solids
#include <BRepOffsetAPI_ThruSections.hxx>

#include <TopExp_Explorer.hxx>

#include <BRepAlgoAPI_Fuse.hxx> // fuse shapes

#include <BRep_Tool.hxx>  // tools for access the geometric properties of shape
#include <BRepLib.hxx>

#include <BRep_Builder.hxx>


TopoDS_Shape MakeBottle(int32_t, int32_t, int32_t);

#endif //SAMPLE_BOTTLE_BOTTLE_H
