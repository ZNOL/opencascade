#include <cstdint>
#include <iostream>

#include "program_options.h"
#include "test.h"

int32_t main(int ac, char* av[]) {
  try {
    auto vars = ParseProgramOptions(ac, av);

    auto &inputFile = vars["inputFile"].as<std::string>();
    auto &outputFile = vars["outputFile"].as<std::string>();

    auto reader = ReadStep(inputFile);
    reader.TransferRoot();

    for (Standard_Integer idx = 1; idx <= reader.NbShapes(); ++idx) {
      Test228(reader.Shape(idx), idx, inputFile, outputFile);
    }
  } catch (const std::exception &exc) {
    std::cerr << "error: " << exc.what() << std::endl;
    return 1;
  }

  return 0;
}