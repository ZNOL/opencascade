#include "test.h"

TopoDS_Shape CreateSphere(uint32_t radius, int32_t x, int32_t y, int32_t z) {
  gp_Pnt center(x, y, z);
  gp_Pnt bottom(x, y, z - static_cast<double>(radius));
  gp_Pnt left(x - static_cast<double>(radius), y, z);

  BRepPrimAPI_MakeSphere mkSphere(center, radius);

  gp_Dir upDir(0, 0, 1);
  gp_Dir leftDir(1, 0, 0);
  gp_Ax2 upAxis(bottom, upDir);
  gp_Ax2 leftAxis(left, leftDir);

  BRepPrimAPI_MakeCylinder mkCylinderUp(upAxis, static_cast<double>(radius) / 4.0, 2 * radius);
  BRepPrimAPI_MakeCylinder mkCylinderLeft(leftAxis, static_cast<double>(radius) / 4.0, 2 * radius);

  auto sphere1 = BRepAlgoAPI_Cut(mkSphere.Shape(), mkCylinderUp.Shape());
  auto sphere = BRepAlgoAPI_Cut(sphere1, mkCylinderLeft.Shape());

  return sphere;
}

void Test228(const TopoDS_Shape &curShape, int32_t idx, const std::string &inputFile, const std::string &outputFile) {
  auto sphere = CreateSphere(1, 0, 0, 0);

  // -----------------------------------------------------------------------
  auto normShape  = utils::NormalizeShape(curShape);

  TopoDS_Compound result5;
  BRep_Builder builder5;
  builder5.MakeCompound(result5);
  builder5.Add(result5, normShape);
  builder5.Add(result5, sphere);

  WriteStep(result5, outputFile + std::to_string(idx) + "_normalized.step");
  // -----------------------------------------------------------------------
  auto shape = transform::RotateShape(curShape, gp::OY(), 30 * M_PI / 180);

  TopoDS_Compound result1;
  BRep_Builder builder1;
  builder1.MakeCompound(result1);
  builder1.Add(result1, sphere);
  builder1.Add(result1, shape);

  WriteStep(result1, outputFile + std::to_string(idx) + "_rotated.step");

  // -----------------------------------------------------------------------
  shape = transform::MoveShape(curShape, 10, -10, 10);

  TopoDS_Compound result2;
  BRep_Builder builder2;
  builder2.MakeCompound(result2);
  builder2.Add(result2, sphere);
  builder2.Add(result2, shape);

  WriteStep(result2, outputFile + std::to_string(idx) + "_moved.step");

  // -----------------------------------------------------------------------
  auto dir = utils::GetMostCommonDir(curShape);

  std::cout << "Most common dir = ";
  Print(std::cout, dir);
  std::cout << std::endl;

//  WriteStep(result3, outputFile + std::to_string(idx) + "_cylindrical.step");

  // -----------------------------------------------------------------------
  auto points = utils::GetBoundaryBox(curShape);

  BRepPrimAPI_MakeBox box(points.min, points.max);

  TopoDS_Compound result4;
  BRep_Builder builder4;
  builder4.MakeCompound(result4);
  builder4.Add(result4, sphere);
  builder4.Add(result4, curShape);
  builder4.Add(result4, box.Shape());

  WriteStep(result4, outputFile + std::to_string(idx) + "_boundaryBox.step");
  // -----------------------------------------------------------------------
}