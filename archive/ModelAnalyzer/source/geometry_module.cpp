#include "geometry_module.h"

namespace geometry {
    bool Equal(Standard_Real a, Standard_Real b) {
      return fabs(a - b) < EPS;
    }

    bool LessOrEqual(Standard_Real a, Standard_Real b) {
      return a < b || Equal(a, b);
    }

    bool Less(Standard_Real a, Standard_Real b) {
      return a < b && !Equal(a, b);
    }

    bool GreaterOrEqual(Standard_Real a, Standard_Real b) {
      return a > b || Equal(a, b);
    }

    bool Greater(Standard_Real a, Standard_Real b) {
      return a > b && !Equal(a, b);
    }
}

template<>
void Print(std::ostream &out, const gp_Ax1 &obj) {
  Print(out, obj.Location());
  out << " | ";
  Print(out, obj.Direction());
}

template<>
bool compare::Equal(const gp_Ax1 &l, const gp_Ax1 &r) {
  if (!Equal(l.Location(), r.Location())) {
    return Less(l.Location(), r.Location());
  } else {
    return Less(l.Direction(), r.Direction());
  }
}

template<>
bool compare::Less(const gp_Ax1 &l, const gp_Ax1 &r) {
  if (!Equal(l.Location(), r.Location())) {
    return Less(l.Location(), r.Location());
  } else {
    return Less(l.Direction(), r.Direction());
  }
}

gp_Pnt operator+(const gp_Pnt &l, const gp_Pnt &r) {
  return {l.X() + r.X(), l.Y() + r.Y(), l.Z() + r.Z()};
}

gp_Pnt operator-(const gp_Pnt &l, const gp_Pnt &r) {
  return {l.X() - r.X(), l.Y() - r.Y(), l.Z() - r.Z()};
}

gp_Dir operator+(const gp_Dir &l, const gp_Dir &r) {
  return {l.X() + r.X(), l.Y() + r.Y(), l.Z() + r.Z()};
}

gp_Dir operator-(const gp_Dir &l, const gp_Dir &r) {
  return {l.X() - r.X(), l.Y() - r.Y(), l.Z() - r.Z()};
}
