#include "program_options.h"


po::variables_map ParseProgramOptions(int ac, char *av[]) {
  po::options_description desc("Allowed options");
  desc.add_options()
          ("help,H", "produce help message")
          ("inputFile,I", po::value<std::string>(), "input .stp file")
          ("outputFile,O", po::value<std::string>()->default_value("./"), "output path")
          ;

  po::variables_map vm;
  po::store(po::parse_command_line(ac, av, desc), vm);
  po::notify(vm);

  if (vm.count("help")) {
    std::cout << desc << std::endl;
  }

  if (!vm.count("inputFile")) {
    throw std::logic_error("no inputFile");
  }

  return vm;
}
