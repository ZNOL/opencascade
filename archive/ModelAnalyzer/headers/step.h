#ifndef SHAPE_EXPOTER_STEP_READER_H
#define SHAPE_EXPOTER_STEP_READER_H

#include <STEPControl_Reader.hxx>
#include <STEPExport.hxx>

STEPControl_Reader ReadStep(const std::string &);

void WriteStep(const TopoDS_Shape &, const std::string &);

#endif //SHAPE_EXPOTER_STEP_READER_H
