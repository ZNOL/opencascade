#ifndef MODEL_ANALYZER_SHAPE_UTILS_H
#define MODEL_ANALYZER_SHAPE_UTILS_H

#include <map>
#include <vector>

#include <TopoDS.hxx>
#include <TopExp_Explorer.hxx>

#include <BRep_Tool.hxx>
#include <BRepBndLib.hxx>
#include <BRep_Builder.hxx>

#include <Geom_CylindricalSurface.hxx>
#include <BRepAdaptor_Surface.hxx>

#include "geometry_module.h"
#include "transform.h"

namespace utils {
    std::vector<gp_Ax1> GetAllAxes(const TopoDS_Shape &) noexcept;

    struct TwoPoints {
        gp_Pnt min;
        gp_Pnt max;

        TwoPoints(
                Standard_Real x1, Standard_Real y1, Standard_Real z1,
                Standard_Real x2, Standard_Real y2, Standard_Real z2
        ) : min(x1, y1, z1), max(x2, y2, z2) {}
    };

    gp_Dir GetMostCommonDir(const TopoDS_Shape &);

    TwoPoints GetBoundaryBox(const TopoDS_Shape &);

    TopoDS_Shape NormalizeShape(TopoDS_Shape);
}

#endif //MODEL_ANALYZER_SHAPE_UTILS_H
