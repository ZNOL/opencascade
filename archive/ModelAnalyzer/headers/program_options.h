#ifndef SIMPLE_OBJECTS_PROGRAM_OPTIONS_H
#define SIMPLE_OBJECTS_PROGRAM_OPTIONS_H

#include <iostream>

#include <boost/program_options.hpp>

namespace po = boost::program_options;

po::variables_map ParseProgramOptions(int, char *[]);

#endif //SIMPLE_OBJECTS_PROGRAM_OPTIONS_H
