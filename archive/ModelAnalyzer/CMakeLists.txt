cmake_minimum_required(VERSION 3.24)

set(PROJECT_NAME "model_analyzer")

project(${PROJECT_NAME})

set(CMAKE_CXX_STANDARD 20)

set(BOOST_HEADERS /home/znol/work/opencascade/third_party/boost)
set(BOOST_LIBS /home/znol/work/opencascade/third_party/boost/stage/lib)

find_package(OpenCASCADE REQUIRED)

include(${OpenCASCADE_DIR}/OpenCASCADEConfig.cmake)

include_directories(./headers)

include_directories(/usr/local/include/opencascade)

include_directories(/usr/local/include/occutils)

include_directories(${BOOST_HEADERS})
include_directories(${BOOST_LIBS})

add_executable(${PROJECT_NAME}
        source/main.cpp
        source/program_options.cpp
        source/step.cpp
        source/geometry_module.cpp
        source/shape_utils.cpp
        source/transform.cpp
        source/test.cpp
        )


target_link_libraries(${PROJECT_NAME} PUBLIC occutils)
message(STATUS " Linked /usr/local/liboccutils.so")

target_link_libraries(${PROJECT_NAME} PUBLIC boost_program_options)
message(STATUS " Linked boost_program_options")

message(STATUS " Started linking OCCT")
file(GLOB ALL_LIBS ${OpenCASCADE_LIBRARY_DIR}/libT*.*)
foreach(LIB ${ALL_LIBS})
  target_link_libraries(${PROJECT_NAME} PUBLIC ${LIB})
  message(STATUS " Linked ${LIB}")
endforeach()
