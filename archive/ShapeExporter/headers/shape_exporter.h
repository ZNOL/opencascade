#ifndef SHAPE_EXPORTER_SHAPE_EXPORTER_H
#define SHAPE_EXPORTER_SHAPE_EXPORTER_H

#include <stdexcept>

#include <TopoDS.hxx>

#include <TopExp_Explorer.hxx>

#include <STEPControl_Reader.hxx>

#include "step_reader.h"

#include "json_export.h"

#endif // SHAPE_EXPORTER_SHAPE_EXPORTER_H