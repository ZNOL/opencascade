#ifndef SHAPE_EXPOTER_JSON_EXPORT_H
#define SHAPE_EXPOTER_JSON_EXPORT_H

#include <string>

#include <TopoDS.hxx>

#include "ShapeTesselator.h"

void ExportToJson(const TopoDS_Shape &, Standard_Integer, Standard_Integer, const std::string &);

#endif //SHAPE_EXPOTER_JSON_EXPORT_H
