#include "step_reader.h"

STEPControl_Reader ReadStep(const std::string &filePath) {
  STEPControl_Reader reader;
  if (reader.ReadFile(filePath.c_str()) != IFSelect_RetDone) {
    throw std::logic_error("cannot open step file");
  }
  return reader;
}