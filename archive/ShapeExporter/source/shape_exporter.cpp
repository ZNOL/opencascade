#include "shape_exporter.h"

void ShapeExporter(const std::string &stepInput, const std::string &jsonOutput) {
  auto reader = ReadStep(stepInput);
  reader.TransferRoot();

  auto numberOfShapes = reader.NbShapes();
  if (numberOfShapes == 0) {
    throw std::logic_error("no shapes found in file");
  }

  TopoDS_Shape shape;
  for (Standard_Integer bodyIdx = 1; bodyIdx <= numberOfShapes; ++bodyIdx) {
    shape = reader.Shape(bodyIdx);

    TopExp_Explorer exp, edge_exp;
    Standard_Integer shapeIdx = 1, edgeIdx = 1;
    for (exp.Init(shape, TopAbs_FACE); exp.More(); exp.Next(), ++shapeIdx) {
      if (!exp.Current().IsNull()) {
        ExportToJson(exp.Current(), bodyIdx, shapeIdx, jsonOutput);

        edgeIdx = 1;
        for (edge_exp.Init(exp.Current(), TopAbs_EDGE); edge_exp.More(); edge_exp.Next(), ++edgeIdx) {
          ExportToJson(edge_exp.Current(), bodyIdx, edgeIdx, jsonOutput);
        }
      }
    }
  }
}