#include "json_export.h"

void ExportToJson(
        const TopoDS_Shape &shape,
        const Standard_Integer bodyIdx,
        const Standard_Integer elemIdx,
        const std::string &folderPath
        ) {
  std::string result;
  std::stringstream uuid;
  if (shape.ShapeType() == TopAbs_FACE) {
    ShapeTesselator tess(shape);
    tess.Compute(false, 1.0, true);

    uuid << "shape_" << bodyIdx << elemIdx << shape.HashCode(INT_MAX);

    result = tess.ExportShapeToThreejsJSONString(uuid.str());
  } else if (shape.ShapeType() == TopAbs_EDGE) {
    uuid << "edge_" << bodyIdx << elemIdx << shape.HashCode(INT_MAX);

    result = ExportEdgeToJSONString(shape, uuid.str());
  } else {
    throw std::logic_error("this type of shape is not supported yet");
  }

  if (result.empty()) {
    throw std::logic_error("failed export to json: result is empty");
  }

  auto filename = folderPath + uuid.str() + ".json";

  std::ofstream out(filename, std::ios::out);
  if (out.is_open()) {
    out << result;
  } else {
    throw std::logic_error("cannot create new file");
  }

}