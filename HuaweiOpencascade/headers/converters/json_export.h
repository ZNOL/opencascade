#ifndef SHAPE_EXPOTER_JSON_EXPORT_H
#define SHAPE_EXPOTER_JSON_EXPORT_H

#include <string>

#include <TopoDS.hxx>
#include <TopExp_Explorer.hxx>
#include <STEPControl_Reader.hxx>

#include "shape_tesselator.h"

namespace convert {
  void ExportToJson(const TopoDS_Shape &, Standard_Integer, Standard_Integer, const std::string &);

  void ShapeExporter(const TopoDS_Shape &, Standard_Integer, const std::string &);

  void ShapeExporter(STEPControl_Reader &, const std::string &);
};

#endif //SHAPE_EXPOTER_JSON_EXPORT_H
