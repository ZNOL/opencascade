#ifndef HUAWEI_OPENCASCADE_RECOGNIZER_H
#define HUAWEI_OPENCASCADE_RECOGNIZER_H

#include <string>

#include <Standard.hxx>
#include <TopoDS_Shape.hxx>
#include <BRepBuilderAPI_Copy.hxx>

#include <asiAlgo_AAG.h>

#include <asiAlgo_RecognizeDrillHoles.h>
#include <asiAlgo_RecognizeCavities.h>
#include <asiAlgo_RecognizeBlends.h>
#include <asiAlgo_RecognizeIsolated.h>

// TODO: check this
#include <asiAlgo_RecognizeEBF.h>
#include <asiAlgo_RecognizeVBF.h>

#include "converters/step.h"

class Recognizer {
public:
    explicit Recognizer(TopoDS_Shape &otherShape);

    explicit Recognizer(const std::string &);

    void EvaluateGraph();

    void RecognizeHoles();

    void RecognizeCavities();

    void RecognizeBlends();

    void RecognizeIsolated();

private:
    TopoDS_Shape shape;
    Handle(asiAlgo_AAG) graph;
};


#endif //HUAWEI_OPENCASCADE_RECOGNIZER_H
