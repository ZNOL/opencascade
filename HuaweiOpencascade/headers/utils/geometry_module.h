#ifndef MODEL_ANALYZER_GEOMETRY_MODULE_H
#define MODEL_ANALYZER_GEOMETRY_MODULE_H


#include <cmath>
#include <iostream>

#include <Standard.hxx>
#include <gp_Pnt.hxx>
#include <gp_Ax1.hxx>

constexpr Standard_Real EPS = 1e-9;
constexpr Standard_Integer PRINT_PRECISION = 9;

template<typename T>
void Print(std::ostream &out, const T &obj) {
  out.precision(PRINT_PRECISION);
  out << std::fixed << "{ " << obj.X() << " , " << obj.Y() << " , " << obj.Z() << " }";
}

namespace geometry {
    bool Equal(Standard_Real, Standard_Real);

    bool LessOrEqual(Standard_Real, Standard_Real);

    bool Less(Standard_Real, Standard_Real);

    bool GreaterOrEqual(Standard_Real, Standard_Real);

    bool Greater(Standard_Real, Standard_Real);
}

namespace compare {
    template<typename T>
    bool Equal(const T &l, const T &r) {
      if (!geometry::Equal(l.X(), r.X()) ||
          !geometry::Equal(l.Y(), r.Y()) ||
          !geometry::Equal(l.Z(), r.Z())
              ) {
        return false;
      }  else {
        return true;
      }
    }

    template<typename T>
    bool Less(const T &l, const T &r) {
      if (!geometry::Equal(l.X(), r.X())) {
        return geometry::Less(l.X(), r.X());
      } else if (!geometry::Equal(l.Y(), r.Y())) {
        return geometry::Less(l.Y(), r.Y());
      } else {
        return geometry::Less(l.Z(), r.Z());
      }
    }
}

gp_Pnt operator+(const gp_Pnt &, const gp_Pnt &);

gp_Pnt operator-(const gp_Pnt &, const gp_Pnt &);

gp_Dir operator+(const gp_Dir &, const gp_Dir &);

gp_Dir operator-(const gp_Dir &, const gp_Dir &);


#endif //MODEL_ANALYZER_GEOMETRY_MODULE_H
