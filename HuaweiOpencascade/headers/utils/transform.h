#ifndef MODEL_ANALYZER_TRANSFORM_H
#define MODEL_ANALYZER_TRANSFORM_H

#include <TopoDS.hxx>

#include <BRepBuilderAPI_Transform.hxx>

namespace transform {
    Standard_Real RadianToDegree(const Standard_Real &) noexcept;

    Standard_Real DegreeToRadian(const Standard_Real &) noexcept;

    TopoDS_Shape RotateShape(const TopoDS_Shape &, const gp_Ax1 &, Standard_Real);

    TopoDS_Shape MoveShape(const TopoDS_Shape &, const gp_Pnt &);

    TopoDS_Shape MoveShape(const TopoDS_Shape &, Standard_Real, Standard_Real, Standard_Real);
}

#endif //MODEL_ANALYZER_TRANSFORM_H
