#ifndef HUAWEI_OPENCASCADE_HUAWEIOPENCASCADE_H
#define HUAWEI_OPENCASCADE_HUAWEIOPENCASCADE_H

#include <TopoDS.hxx>
#include <BRepBuilderAPI_Copy.hxx>

#include "converters/step.h"
#include "converters/json_export.h"
#include "utils/shape_utils.h"

class HuaweiOpenCascade {
public:
    // Constructors
    HuaweiOpenCascade() = default;

    explicit HuaweiOpenCascade(const std::string &);

    HuaweiOpenCascade(const HuaweiOpenCascade &);

    // Transform part
    void RotateShape(const gp_Ax1 &, Standard_Real);

    void MoveShape(const gp_Pnt &);

    void MoveShape(Standard_Real, Standard_Real, Standard_Real);

    // Shape utils part
    [[nodiscard]] std::vector<gp_Ax1> GetAllAxes() const;

    [[nodiscard]] gp_Dir GetMostCommonDir() const;

    [[nodiscard]] utils::TwoPoints GetBoundaryBox() const;

    void NormalizeShape();

    [[nodiscard]] utils::SurfacesMap GetQuantityOfSurfaces() const;

    // Export part
    void ExportToJson(const std::string &);

    void ExportToStep(const std::string &);
private:
    TopoDS_Shape shape;
};


#endif //HUAWEI_OPENCASCADE_HUAWEIOPENCASCADE_H
