#ifndef MODEL_ANALYZER_TEST_H
#define MODEL_ANALYZER_TEST_H

#include <BRepPrimAPI_MakeSphere.hxx>
#include <BRepPrimAPI_MakeCylinder.hxx>
#include <BRepAlgoAPI_Cut.hxx>
#include <BRepPrimAPI_MakeBox.hxx>

#include "utils/shape_utils.h"
#include "utils/transform.h"
#include "converters/step.h"

TopoDS_Shape CreateSphere(uint32_t, int32_t, int32_t, int32_t);

void Test228(const TopoDS_Shape &, int32_t , const std::string &, const std::string &);

#endif //MODEL_ANALYZER_TEST_H
