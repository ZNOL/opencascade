#include "recognizer/Recognizer.h"

#include <stdint.h>
#include <iostream>

void Test_Holes() {
  auto rec = Recognizer("../tests/recognizer/step/holes.step");
  rec.EvaluateGraph();
  rec.RecognizeHoles();
}

void Test_Cavities() {
  auto rec = Recognizer("../tests/recognizer/step/holes.step");
  rec.EvaluateGraph();
  rec.RecognizeCavities();
}

void Test_Blends() {
  auto rec = Recognizer("../tests/recognizer/step/blends.stp");
  rec.EvaluateGraph();
  rec.RecognizeBlends();
}

void Test_Isolated() {
  auto rec = Recognizer("../tests/recognizer/step/holes.step");
  rec.EvaluateGraph();
  rec.RecognizeIsolated();
}

int32_t main() {
  Test_Holes();
  Test_Cavities();
  Test_Blends();
  Test_Isolated();

  return 0;
}