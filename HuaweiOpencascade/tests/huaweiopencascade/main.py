import huaweiopencascade

model = huaweiopencascade.HuaweiOpenCascade("../../cmake-build-debug/bodies/rotated_par.step")

model.ExportToStep("./step_file.stp")

model.NormalizeShape()

model.ExportToStep("./step_file_norm.stp")

print(model.GetAllAxes())