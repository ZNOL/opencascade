#include <cstdint>
#include <iostream>

#include "program_options.h"

#include "converters/step.h"
#include "utils/shape_utils.h"

int32_t main(int ac, char *av[]) {
  try {
    auto vars = ParseProgramOptions(ac, av);

    auto &inputFile = vars["inputFile"].as<std::string>();
    auto &outputFile = vars["outputFile"].as<std::string>();

    auto reader = ReadStep(inputFile);
    reader.TransferRoot();

    for (Standard_Integer idx = 1; idx <= reader.NbShapes(); ++idx) {
      auto shape = utils::NormalizeShape(reader.Shape(idx));

      WriteStep(shape,
        outputFile + std::to_string(idx) + "_" + std::to_string(shape.HashCode(INT_MAX)) + ".stp");
    }
  } catch (const std::exception &exc) {
    std::cerr << "error: " << exc.what() << std::endl;
    return 1;
  }

  return 0;
}