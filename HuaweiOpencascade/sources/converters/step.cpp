#include "converters/step.h"

STEPControl_Reader ReadStep(const std::string &filePath) {
  STEPControl_Reader reader;
  if (reader.ReadFile(filePath.c_str()) != IFSelect_RetDone) {
    throw std::logic_error("cannot open step file");
  }
  return reader;
}

void WriteStep(const TopoDS_Shape &shape, const std::string &filename) {
  OCCUtils::STEP::ExportSTEP(shape, filename);
}
