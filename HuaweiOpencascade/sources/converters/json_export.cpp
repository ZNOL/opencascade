#include "converters/json_export.h"

void convert::ExportToJson(
        const TopoDS_Shape &shape,
        const Standard_Integer bodyIdx,
        const Standard_Integer elemIdx,
        const std::string &folderPath
        ) {
  std::string result;
  std::stringstream uuid;
  if (shape.ShapeType() == TopAbs_FACE) {
    ShapeTesselator tess(shape);
    tess.Compute(/*compute_edges=*/ false, /*mesh_quality=*/1.0, /*parallel=*/true);

    uuid << "shape_" << bodyIdx << elemIdx << shape.HashCode(INT_MAX);

    result = tess.ExportShapeToThreejsJSONString(uuid.str());
  } else if (shape.ShapeType() == TopAbs_EDGE) {
    uuid << "edge_" << bodyIdx << elemIdx << shape.HashCode(INT_MAX);

    result = ExportEdgeToJSONString(shape, uuid.str());
  } else {
    throw std::logic_error("this type of shape is not supported yet");
  }

  if (result.empty()) {
    throw std::logic_error("failed converters to json: result is empty");
  }

  auto filename = folderPath + uuid.str() + ".json";

  std::ofstream out(filename, std::ios::out);
  if (out.is_open()) {
    out << result;
  } else {
    throw std::logic_error("cannot create new file");
  }

}

void convert::ShapeExporter(const TopoDS_Shape &shape, Standard_Integer bodyIdx, const std::string &jsonOutput) {
  TopExp_Explorer exp, edge_exp;
  Standard_Integer shapeIdx = 1, edgeIdx = 1;
  for (exp.Init(shape, TopAbs_FACE); exp.More(); exp.Next(), ++shapeIdx) {
    if (exp.Current().IsNull()) {
      continue;
    }

    convert::ExportToJson(exp.Current(), bodyIdx, shapeIdx, jsonOutput);

    edgeIdx = 1;
    for (edge_exp.Init(exp.Current(), TopAbs_EDGE); edge_exp.More(); edge_exp.Next(), ++edgeIdx) {
      convert::ExportToJson(edge_exp.Current(), bodyIdx, edgeIdx, jsonOutput);
    }
  }
}

void convert::ShapeExporter(STEPControl_Reader &reader, const std::string &jsonOutput) {
  reader.TransferRoot();

  auto numberOfShapes = reader.NbShapes();
  if (numberOfShapes == 0) {
    throw std::logic_error("no shapes found in file");
  }

  TopoDS_Shape shape;
  for (Standard_Integer bodyIdx = 1; bodyIdx <= numberOfShapes; ++bodyIdx) {
    shape = reader.Shape(bodyIdx);
    convert::ShapeExporter(shape, bodyIdx, jsonOutput);
  }
}