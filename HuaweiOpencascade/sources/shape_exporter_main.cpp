#include <cstdint>

#include "program_options.h"

#include "converters/step.h"
#include "converters/json_export.h"

int32_t main(int ac, char *av[]) {
  try {
    auto vars = ParseProgramOptions(ac, av);
    auto &inputFile = vars["inputFile"].as<std::string>();
    auto outputFile = vars["outputFile"].as<std::string>();

    if (outputFile.empty()) {
      outputFile = GetFolder(inputFile);
    }

    auto reader = ReadStep(inputFile);

    convert::ShapeExporter(reader, outputFile);
  } catch (const std::exception &exc) {
    std::cerr << "error: " << exc.what() << std::endl;
    return 1;
  }

  return 0;
}