#include "recognizer/Recognizer.h"

#include <iostream>

#include <converters/json_export.h>

Recognizer::Recognizer(TopoDS_Shape &other) : graph(nullptr) {
  BRepBuilderAPI_Copy copy;
  copy.Perform(other);
  shape = copy.Shape();
}

Recognizer::Recognizer(const std::string &filePath) : graph(nullptr) {
  auto reader = ReadStep(filePath);
  reader.TransferRoot();
  shape = reader.Shape();
}

void Recognizer::EvaluateGraph() {
  graph = new asiAlgo_AAG(shape);
}

void Recognizer::RecognizeHoles() {
  if (graph.IsNull()) {
    throw std::logic_error("AAG is not ready yet");
  }

  asiAlgo_RecognizeDrillHoles recHoles(graph, /*doHard=*/true);
  recHoles.Perform();  // TODO: here can be passed max hole radius

  const asiAlgo_Feature &resIndexes = recHoles.GetResultIndices();

  resIndexes.Statistics(std::cout);

  for (asiAlgo_Feature::Iterator it(resIndexes); it.More(); it.Next()) {
    std::cout << it.Key() << " ";
    auto item = graph->GetFace(it.Key());
    convert::ShapeExporter(item, it.Key(), "../tests/recognizer/result/holes/");
  }
  std::cout << std::endl;
}

void Recognizer::RecognizeCavities() {
  if (graph.IsNull()) {
    throw std::logic_error("AAG is not ready yet");
  }

  asiAlgo_RecognizeCavities recCavities(graph);
  recCavities.Perform();

  const asiAlgo_Feature &resIndexes = recCavities.GetResultIndices();

  resIndexes.Statistics(std::cout);

  for (asiAlgo_Feature::Iterator it(resIndexes); it.More(); it.Next()) {
    std::cout << it.Key() << " ";
    auto item = graph->GetFace(it.Key());
    convert::ShapeExporter(item, it.Key(), "../tests/recognizer/result/cavities/");
  }
  std::cout << std::endl;
}

void Recognizer::RecognizeBlends() {
  if (graph.IsNull()) {
    throw std::logic_error("AAG is not ready yet");
  }

  asiAlgo_RecognizeBlends recBlends(graph);
  recBlends.Perform();  // TODO: here can be passed radius of interest

  const asiAlgo_Feature &resIndexes = recBlends.GetResultIndices();

  resIndexes.Statistics(std::cout);

  for (asiAlgo_Feature::Iterator it(resIndexes); it.More(); it.Next()) {
    std::cout << it.Key() << " ";
    auto item = graph->GetFace(it.Key());
    convert::ShapeExporter(item, it.Key(), "../tests/recognizer/result/blends/");
  }
  std::cout << std::endl;
}

void Recognizer::RecognizeIsolated() {  // TODO: add seed (=idx of face to start from)
  if (graph.IsNull()) {
    throw std::logic_error("AAG is not ready yet");
  }

  asiAlgo_RecognizeIsolated recIsolated(graph);

  asiAlgo_Feature seed;
  for (int i = 1; i <= 20; ++i) {
    seed.Add(i);  // TODO: change
  }
  recIsolated.Perform(seed);

  const asiAlgo_Feature &resIndexes = recIsolated.GetResultIndices();

  resIndexes.Statistics(std::cout);

  for (asiAlgo_Feature::Iterator it(resIndexes); it.More(); it.Next()) {
    std::cout << it.Key() << " ";
    auto item = graph->GetFace(it.Key());
    convert::ShapeExporter(item, it.Key(), "../tests/recognizer/result/isolated/");
  }
  std::cout << std::endl;
}


