#include <boost/python.hpp>

#include "boost_python/python.h"
#include "boost_python/HuaweiOpencascade.h"

using namespace boost::python;

BOOST_PYTHON_MODULE(huaweiopencascade) {
  class_<HuaweiOpenCascade>("HuaweiOpenCascade")
          .def(init<std::string>(args("filepath")))
          .def("RotateShape", static_cast<void (HuaweiOpenCascade::*)(const gp_Ax1&, Standard_Real)>(&HuaweiOpenCascade::RotateShape), args("axis", "ang"))
          .def("MoveShape", static_cast<void (HuaweiOpenCascade::*)(const gp_Pnt&)>(&HuaweiOpenCascade::MoveShape), args("point"))
          .def("MoveShape", static_cast<void (HuaweiOpenCascade::*)(Standard_Real, Standard_Real, Standard_Real)>(&HuaweiOpenCascade::MoveShape), args("dx", "dy", "dz"))
          .def("GetBoundaryBox", &HuaweiOpenCascade::GetBoundaryBox)
          .def("GetMostCommonDir", &HuaweiOpenCascade::GetMostCommonDir)
          .def("GetAllAxes", &HuaweiOpenCascade::GetAllAxes)
          .def("NormalizeShape", &HuaweiOpenCascade::NormalizeShape)
          .def("GetQuantityOfSurface", &HuaweiOpenCascade::GetQuantityOfSurfaces)
          .def("ExportToJson", &HuaweiOpenCascade::ExportToJson, args("folderpath"))
          .def("ExportToStep", &HuaweiOpenCascade::ExportToStep, args("folderpath"))
          ;
}
