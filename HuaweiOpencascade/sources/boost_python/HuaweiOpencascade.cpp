#include "boost_python/HuaweiOpencascade.h"

HuaweiOpenCascade::HuaweiOpenCascade(const std::string &filePath) {
  auto reader = ReadStep(filePath);
  reader.TransferRoot();
  shape = reader.Shape();
}

HuaweiOpenCascade::HuaweiOpenCascade(const HuaweiOpenCascade &other) {
  BRepBuilderAPI_Copy copy;
  copy.Perform(other.shape);
  shape = copy.Shape();
}

void HuaweiOpenCascade::RotateShape(const gp_Ax1 &axis, const Standard_Real ang) {
  shape = transform::RotateShape(shape, axis, ang);
}

void HuaweiOpenCascade::MoveShape(const gp_Pnt &point) {
  shape = transform::MoveShape(shape, point);
}

void HuaweiOpenCascade::MoveShape(Standard_Real dx, Standard_Real dy, Standard_Real dz) {
  shape = transform::MoveShape(shape, dx, dy, dz);
}

utils::TwoPoints HuaweiOpenCascade::GetBoundaryBox() const {
  return utils::GetBoundaryBox(shape);
}

gp_Dir HuaweiOpenCascade::GetMostCommonDir() const {
  return utils::GetMostCommonDir(shape);
}

std::vector<gp_Ax1> HuaweiOpenCascade::GetAllAxes() const {
  return utils::GetAllAxes(shape);
}

void HuaweiOpenCascade::NormalizeShape() {
  shape = utils::NormalizeShape(shape);
}

utils::SurfacesMap HuaweiOpenCascade::GetQuantityOfSurfaces() const {
  return utils::GetQuantityOfSurfaces(shape);
}

void HuaweiOpenCascade::ExportToJson(const std::string &folderPath) {
  convert::ShapeExporter(shape, 1, folderPath);
}

void HuaweiOpenCascade::ExportToStep(const std::string &folderPath) {
  WriteStep(shape, folderPath);
}

