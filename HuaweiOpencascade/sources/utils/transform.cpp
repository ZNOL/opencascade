#include "utils/transform.h"

Standard_Real transform::RadianToDegree(const Standard_Real &radian) noexcept {
  static constexpr Standard_Real factor = 180.0 / M_PI;
  return radian * factor;
}

Standard_Real transform::DegreeToRadian(const Standard_Real &degree) noexcept {
  static constexpr Standard_Real factor = M_PI / 180.0;
  return degree * factor;
}

TopoDS_Shape transform::RotateShape(const TopoDS_Shape &shape, const gp_Ax1 &axis, const Standard_Real ang) {
  gp_Trsf matrix;
  matrix.SetRotation(axis, ang);

  BRepBuilderAPI_Transform transform(shape, matrix, true, true);

  return transform.Shape();
}

TopoDS_Shape transform::MoveShape(const TopoDS_Shape &shape, const gp_Pnt &point) {
  return transform::MoveShape(shape, point.X(), point.Y(), point.Z());
}

TopoDS_Shape transform::MoveShape(const TopoDS_Shape &shape, Standard_Real dx, Standard_Real dy, Standard_Real dz) {
  gp_Trsf matrix;
  matrix.SetValues(
    1, 0, 0, dx,
    0, 1, 0, dy,
    0, 0, 1, dz
  );

  BRepBuilderAPI_Transform transform(shape, matrix);

  return transform.Shape();
}

