#include "utils/shape_utils.h"

std::vector<gp_Ax1> utils::GetAllAxes(const TopoDS_Shape &shape) noexcept {
  std::vector<gp_Ax1> result;

  TopExp_Explorer exp;
  for (exp.Init(shape, TopAbs_FACE); exp.More(); exp.Next()) {
    BRepAdaptor_Surface surface(TopoDS::Face(exp.Current()));
    switch (surface.GetType()) {
      case GeomAbs_Cylinder:
        result.push_back(surface.Cylinder().Axis());
        break;
      case GeomAbs_Cone:
        result.push_back(surface.Cone().Axis());
        break;
      case GeomAbs_Torus:
        result.push_back(surface.Torus().Axis());
        break;
      default:
        break;
    }
  }

  return result;
}

gp_Dir utils::GetMostCommonDir(const TopoDS_Shape &shape) {
  auto axes = GetAllAxes(shape);

  auto dirCompare = compare::Less<gp_Dir>;
  std::map<gp_Dir, int32_t, decltype(dirCompare)> dirs{dirCompare};

  int32_t maxAmount = 0;
  gp_Dir mostCommon(gp::DZ());

  dirs[gp::DZ()] = 0;
  for (const auto &axis: axes) {
    auto curDir = axis.Direction();

    if (dirs.find(curDir) == dirs.end()) {
      for (const auto &dir : dirs) {
        if (curDir.IsParallel(dir.first,  EPS)) {
          curDir = dir.first;
          break;
        }
      }
    }

    auto curAmount = ++dirs[curDir];
    if (curAmount > maxAmount) {
      maxAmount = curAmount;
      mostCommon = curDir;
    }
  }

  if (maxAmount == 0) {
    throw std::logic_error("cannot find most common dir");
  }

  if (dirs[gp::DZ()] >= maxAmount) {
    mostCommon = gp::DZ();
  }

  return mostCommon;
}

utils::TwoPoints utils::GetBoundaryBox(const TopoDS_Shape &shape) {
  Bnd_Box boundBox;
  BRepBndLib::Add(shape, boundBox);

  Standard_Real xMin, yMin, zMin, xMax, yMax, zMax;

  boundBox.Get(xMin, yMin, zMin, xMax, yMax, zMax);

  return {xMin, yMin, zMin, xMax, yMax, zMax};
}

TopoDS_Shape utils::NormalizeShape(TopoDS_Shape shape) {
  const static gp_Pnt XYZ{0, 0,0};
  const static gp_Ax1 AZ(XYZ, gp::DZ());
  const static gp_Ax1 AY(XYZ, gp::DY());

  auto mostCommonDir = utils::GetMostCommonDir(shape);
  if (!mostCommonDir.IsEqual(gp::DZ(), EPS) && !mostCommonDir.IsParallel(gp::DZ(), EPS)) {
    Standard_Real angX = gp::DX().Angle(gp_Dir{mostCommonDir.X(), mostCommonDir.Y(), 0});

    shape = transform::RotateShape(shape, AZ, -angX);
    mostCommonDir.Rotate(AZ, -angX);

    Standard_Real angZ = gp::DZ().Angle(mostCommonDir);
    shape = transform::RotateShape(shape, AY, -angZ);
  }
  auto box = utils::GetBoundaryBox(shape);

  Standard_Real dx_2 = fabs(box.min.X() - box.max.X()) * 0.5;
  Standard_Real dy_2 = fabs(box.min.Y() - box.max.Y()) * 0.5;
  gp_Pnt shapeMid = {box.min.X() + dx_2, box.min.Y() + dy_2, box.min.Z()};

  shape = transform::MoveShape(shape, XYZ - shapeMid);

  return shape;
}

utils::SurfacesMap utils::GetQuantityOfSurfaces(const TopoDS_Shape &shape) noexcept {
  utils::SurfacesMap result;

  TopExp_Explorer exp;
  for (exp.Init(shape, TopAbs_FACE); exp.More(); exp.Next()) {
    BRepAdaptor_Surface surface(TopoDS::Face(exp.Current()));
    auto type = surface.GetType();
    ++result[type];
  }

  return result;
}
